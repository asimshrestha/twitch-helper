//go:generate goversioninfo

package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/pkg/browser"

	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"
	"gitlab.com/asimshrestha/twitch-helper/guicontroller"
	"gitlab.com/asimshrestha/twitch-helper/helper"
	"gitlab.com/asimshrestha/twitch-helper/hotkey"
	"gitlab.com/asimshrestha/twitch-helper/save"
	"gitlab.com/asimshrestha/twitch-helper/server"
	"gitlab.com/asimshrestha/twitch-helper/twitchchat"
)

var minimize *bool

func main() {
	minimize = flag.Bool("minimize", false, "to start the application minimized")
	flag.Parse()
	go hotkey.RegisterHotKey()
	go server.StartServer()
	save.LoadSettings()
	twitchchat.RefreshTimer()
	if !save.Setting.IsMissingValues() {
		go twitchchat.StartTracker()
	}
	fmt.Println(*minimize)
	MakeWindow()
}

func MakeWindow() {
	fN, _ := os.Executable()
	fileDir := filepath.Dir(fN)
	walk.Resources.SetRootDirPath(fileDir + `\img`)
	mw := MainWindow{
		AssignTo:   &guicontroller.MW.MainWindow,
		Title:      "Twitch Helper",
		MinSize:    Size{400, 140},
		Size:       Size{400, 200},
		Icon:       "icon.ico",
		Visible:    !*minimize,
		Background: SolidColorBrush{Color: walk.RGB(29, 37, 44)},
		Layout:     VBox{MarginsZero: true},
		MenuItems: []MenuItem{
			Menu{
				Text: "&File",
				Items: []MenuItem{
					Action{
						Text:        "&Reset Timer",
						OnTriggered: func() { go twitchchat.RefreshTimer() },
					},
					Action{
						Text:        "Get Twitch &OAuth Code",
						OnTriggered: func() { browser.OpenURL("https://twitchapps.com/tmi/") },
					},
					Separator{},
					Action{
						Text:        "Exit",
						OnTriggered: func() { guicontroller.MW.Close() },
					},
				},
			},
			Menu{
				Text: "&Help",
				Items: []MenuItem{
					Action{
						Text:        "&Readme",
						OnTriggered: func() { browser.OpenURL("https://gitlab.com/asimshrestha/twitch-helper/blob/master/README.md") },
					},
				},
			},
		},
		Children: []Widget{
			VSpacer{},
			Label{
				Font:      guicontroller.FontSSubTitle,
				Text:      "Enter Twitch Channel:",
				TextColor: walk.RGB(225, 225, 225),
			},
			LineEdit{
				AssignTo:  &guicontroller.MW.TwitchUsername,
				Font:      guicontroller.FontSubTitle,
				Text:      save.Setting.Channel,
				TextColor: walk.RGB(0, 0, 0),
			},
			VSpacer{},
			Label{
				Font:      guicontroller.FontSSubTitle,
				Text:      "Enter Emote Name:",
				TextColor: walk.RGB(225, 225, 225),
			},
			LineEdit{
				AssignTo:  &guicontroller.MW.TwitchEmote,
				Font:      guicontroller.FontSubTitle,
				Text:      save.Setting.Emote,
				TextColor: walk.RGB(0, 0, 0),
			},
			VSpacer{},
			Label{
				Font:      guicontroller.FontSSubTitle,
				Text:      "Enter Oauth Code:",
				TextColor: walk.RGB(225, 225, 225),
			},
			LineEdit{
				AssignTo:     &guicontroller.MW.OAuth,
				Font:         guicontroller.FontSubTitle,
				Text:         save.Setting.OAuth,
				PasswordMode: true,
				TextColor:    walk.RGB(0, 0, 0),
			},
			VSpacer{},
			Label{
				Font:      guicontroller.FontSSubTitle,
				Text:      "Enter Stream Labels Location: ",
				TextColor: walk.RGB(225, 225, 225),
			},
			LineEdit{
				AssignTo:  &guicontroller.MW.StreamLabelsLocation,
				Font:      guicontroller.FontSubTitle,
				Text:      save.Setting.StreamLabelsLoc,
				TextColor: walk.RGB(0, 0, 0),
			},
			VSpacer{},
			Label{
				Font:      guicontroller.FontSSubTitle,
				Text:      "Enter Sub Goal:",
				TextColor: walk.RGB(225, 225, 225),
			},
			LineEdit{
				AssignTo:  &guicontroller.MW.SubGoal,
				Font:      guicontroller.FontSubTitle,
				Text:      strconv.Itoa(save.Setting.SubGoal),
				TextColor: walk.RGB(0, 0, 0),
			},
			// VSpacer{},
			// HSplitter{
			// 	Children: []Widget{
			// 		Label{
			// 			Font:      guicontroller.FontSSubTitle,
			// 			Text:      " Set Hotkey for Que",
			// 			TextColor: walk.RGB(225, 225, 225),
			// 		},
			// 		PushButton{
			// 			Font:      guicontroller.FontSSubTitle,
			// 			Text:      "Set",
			// 			OnClicked: guicontroller.MW.OpenKeyDialog,
			// 		},
			// 	},
			// },
			VSpacer{},
			HSplitter{
				Children: []Widget{
					HSpacer{},
					Label{
						Font:      guicontroller.FontSSubTitle,
						Text:      "Open on Startup",
						TextColor: walk.RGB(225, 225, 225),
					},
					CheckBox{
						AssignTo: &guicontroller.MW.StartUp,
						Font:     guicontroller.FontSSubTitle,
						Checked:  helper.IsRegSet(),
					},
				},
			},
			VSpacer{},
			PushButton{
				AssignTo: &guicontroller.MW.SaveBtn,
				Text:     "Save",
				OnClicked: func() {
					if guicontroller.MW.StartUp.Checked() {
						if !helper.IsRegSet() {
							go helper.SetReg()
						}
					} else {
						if helper.IsRegSet() {
							go helper.RemoveReg()
						}
					}

					ti, err := strconv.Atoi(strings.TrimSpace(guicontroller.MW.SubGoal.Text()))
					if err != nil {
						log.Fatal(err)
						return
					}

					t := save.Settings{
						Channel:         strings.TrimSpace(guicontroller.MW.TwitchUsername.Text()),
						Emote:           strings.TrimSpace(guicontroller.MW.TwitchEmote.Text()),
						OAuth:           strings.TrimSpace(guicontroller.MW.OAuth.Text()),
						StreamLabelsLoc: strings.TrimSpace(guicontroller.MW.StreamLabelsLocation.Text()),
						SubGoal:         ti,
					}

					if !save.Setting.IsEqual(t) {
						save.Setting = t

						go func() {
							save.SaveConfig()
							if twitchchat.IsNotConnected() {
								if !save.Setting.IsMissingValues() {
									twitchchat.StartTracker()
								}
							} else {
								twitchchat.RestartTracker()
							}
						}()
					}
				},
			},
		},
	}

	icon, err := walk.Resources.Icon("icon.ico")
	if err != nil {
		log.Fatal(err)
	}

	ni, err := walk.NewNotifyIcon()
	if err != nil {
		log.Fatal(err)
	}
	defer ni.Dispose()

	if err := ni.SetIcon(icon); err != nil {
		log.Fatal(err)
	}
	if err := ni.SetToolTip("Twitch Helper"); err != nil {
		log.Fatal(err)
	}

	toggleVisibilityAction := walk.NewAction()

	// When the left mouse button is pressed, bring up our balloon.
	ni.MouseDown().Attach(func(x, y int, button walk.MouseButton) {
		if button != walk.LeftButton {
			return
		}

		if !guicontroller.MW.MainWindow.Visible() {
			guicontroller.MW.MainWindow.Show()
			toggleVisibilityAction.SetText("Hide Twitch Helper")
		}
	})

	if err := toggleVisibilityAction.SetText("Hide Twitch Helper"); err != nil {
		log.Fatal(err)
	}
	toggleVisibilityAction.Triggered().Attach(func() {
		if guicontroller.MW.MainWindow.Visible() {
			guicontroller.MW.MainWindow.Hide()
			toggleVisibilityAction.SetText("Show Twitch Helper")
		} else {
			guicontroller.MW.MainWindow.Show()
			toggleVisibilityAction.SetText("Hide Twitch Helper")
		}
	})
	if err := ni.ContextMenu().Actions().Add(toggleVisibilityAction); err != nil {
		log.Fatal(err)
	}

	qAction := walk.NewAction()
	if err := qAction.SetText("Show &Queue Info"); err != nil {
		log.Fatal(err)
	}
	qAction.Triggered().Attach(func() {
		var j = make(map[string]interface{})
		j["show"] = hotkey.ShowQueue
		hotkey.ShowQueue = !hotkey.ShowQueue
		msg, _ := helper.JsonString("queue", j)
		server.AddMessage(msg)
	})
	if err := ni.ContextMenu().Actions().Add(qAction); err != nil {
		log.Fatal(err)
	}

	exitAction := walk.NewAction()
	if err := exitAction.SetText("E&xit"); err != nil {
		log.Fatal(err)
	}
	exitAction.Triggered().Attach(func() { walk.App().Exit(0) })
	if err := ni.ContextMenu().Actions().Add(exitAction); err != nil {
		log.Fatal(err)
	}

	// The notify icon is hidden initially, so we have to make it visible.
	if err := ni.SetVisible(true); err != nil {
		log.Fatal(err)
	}

	if _, err := mw.Run(); err != nil {
		log.Fatal(err)
	}

	guicontroller.MW.MainWindow.SetIcon(icon)
}
