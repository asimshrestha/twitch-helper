package server

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"text/template"

	"github.com/julienschmidt/httprouter"
)

func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	fmt.Fprintf(w, "<html><body>Hi</body></html>")
}

func Layout(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fN, _ := os.Executable()
	fileDir := filepath.Dir(fN)
	webcamTemp, _ := template.ParseFiles(fileDir + "/server/template/layout.html")
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	var v = struct {
		Host string
	}{
		r.Host,
	}
	webcamTemp.Execute(w, &v)
}

// func Notification(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
// 	fN, _ := os.Executable()
// 	fileDir := filepath.Dir(fN)
// 	notificationTemp, _ := template.ParseFiles(fileDir + "/server/template/notification.html")
// 	w.Header().Set("Content-Type", "text/html; charset=utf-8")
// 	var v = struct {
// 		Host string
// 	}{
// 		r.Host,
// 	}
// 	notificationTemp.Execute(w, &v)
// }

// func Queue(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
// 	fN, _ := os.Executable()
// 	fileDir := filepath.Dir(fN)
// 	queueTemp, _ := template.ParseFiles(fileDir + "/server/template/queue.html")
// 	w.Header().Set("Content-Type", "text/html; charset=utf-8")
// 	var v = struct {
// 		Host string
// 	}{
// 		r.Host,
// 	}
// 	queueTemp.Execute(w, &v)
// }
