function play(repeat = 0){
	if(!this.playing){
		this.playing = true
		this.intervalId = setInterval(() => {
			document.getElementById(this.name).style.backgroundPosition = `-${this.pos.x}px -${this.pos.y}px`;
			
			if(this.totalframes != 0 && (this.frame.x + 1) * (this.frame.y + 1) >= this.totalframes){
				this.pos = {x: 0,y: 0};
				this.frame = {x: 0, y: 0};
				if(repeat > 0){
					repeat--;
					console.log(repeat);
					if(repeat <= 0){
						this.stop();
					}
				}
			}
			
			if(this.frame.x < this.col - 1){
				this.frame.x++;
				this.pos.x += (this.width/this.col);
			} else {
				this.pos.x = 0;
				
				if(this.frame.y < this.row - 1){
					this.frame.y++;
					this.pos.y += (this.height/this.row);
				} else {
					this.frame.y = 0;
					this.pos.y = 0;
				}
				this.frame.x = 0;
			}
		}, 1000/this.fps);
	}
}

function pause(){
	if(this.playing){
		this.playing = false
		console.log(this.name + " paused")
		clearInterval(this.intervalId);
		this.intervalId = null;
	}
}

function stop(){
	this.playing = false
	clearInterval(this.intervalId);
	this.intervalId = null;
	this.pos = {x: 0,y: 0};
	this.frame = {x: 0, y: 0};
	document.getElementById(this.name).style.backgroundPosition = "0px 0px";
}

function Sprite(opt = {}){
	this.name = opt.name || opt.container+"img";
	this.width = opt.width || 0;
	this.height = opt.height || 0;
	this.row = opt.row || 1;
	this.col = opt.col || 1;
	this.fps = opt.fps || 1;
	this.url = opt.url || "";
	this.container = opt.container || "";
	this.totalframes = opt.totalframes || 0;
	this.frame = {x: 0, y: 0};
	this.pos = {x: 0,y: 0};
	this.play = play;
	this.pause = pause;
	this.stop = stop;
	this.playing = false;
	this.intervalId = null;
	
	var t = document.createElement("div");
	t.setAttribute('id', this.name);
	t.setAttribute('style', `width: ${this.width/this.col}px; height: ${this.height/this.row}px;
background: url('${this.url}') 0px 0px no-repeat;`)
	document.getElementById(this.container).appendChild(t);
}

//Que Sprite
function qplay(repeat = 0){
	if(!this.playing){
		this.playing = true
		this.intervalId = setInterval(() => {
			document.getElementById(this.name).style.backgroundPosition = `-${this.pos.x}px -${this.pos.y}px`;
			
			if(this.totalframes != 0 && (this.frame.x + 1) * (this.frame.y + 1) >= this.totalframes){
				this.pos = {x: 0,y: 0};
				this.frame = {x: 0, y: 0};
				if(repeat > 0){
					repeat--;
					console.log(repeat);
					if(repeat <= 0){
						this.playing = false
                        clearInterval(this.intervalId);
                        this.intervalId = null;
                        this.pos = {
                            x: this.width - (this.width/this.col),
                            y:  this.height - (this.height/this.row)
                        };
                        this.frame = {x: 0, y: 0};
                        document.getElementById(this.name).style.backgroundPosition = `-${this.pos.x}px -${this.pos.y}px`;
					}
				}
			}
			
			if(this.frame.x < this.col - 1){
				this.frame.x++;
				this.pos.x += (this.width/this.col);
			} else {
				this.pos.x = 0;
				
				if(this.frame.y < this.row - 1){
					this.frame.y++;
					this.pos.y += (this.height/this.row);
				} else {
					this.frame.y = 0;
					this.pos.y = 0;
				}
				this.frame.x = 0;
			}
		}, 1000/this.fps);
	}
}

function qplayreverse(repeat = 0){
	if(!this.playing){
		this.playing = true;
        this.pos.x = this.width - (this.width/this.col);
        this.pos.y = this.height - (this.height/this.row);
		this.intervalId = setInterval(() => {
			document.getElementById(this.name).style.backgroundPosition = `-${this.pos.x}px -${this.pos.y}px`;
			
			if(this.totalframes != 0 && (this.frame.x + 1) * (this.frame.y + 1) >= this.totalframes){
				this.pos = {x: 0,y: 0};
				this.frame = {x: 0, y: 0};
				if(repeat > 0){
					repeat--;
					console.log(repeat);
					if(repeat <= 0){
						this.stop();
					}
				}
			}
			
			if(this.frame.x < this.col - 1){
				this.frame.x++;
				this.pos.x -= (this.width/this.col);
			} else {
				this.pos.x = this.width;
				
				if(this.frame.y < this.row - 1){
					this.frame.y++;
					this.pos.y -= (this.height/this.row);
				} else {
					this.frame.y = 0;
					this.pos.y = this.height;
				}
				this.frame.x = 0;
			}
		}, 1000/this.fps);
	}
}

function qpause(){
	if(this.playing){
		this.playing = false
		console.log(this.name + " paused")
		clearInterval(this.intervalId);
		this.intervalId = null;
	}
}

function qstop(){
	this.playing = false
	clearInterval(this.intervalId);
	this.intervalId = null;
	this.pos = {x: 0,y: 0};
	this.frame = {x: 0, y: 0};
	document.getElementById(this.name).style.backgroundPosition = "0px 0px";
}

function QueSprite(opt = {}){
	this.name = opt.name || opt.container+"img";
	this.width = opt.width || 0;
	this.height = opt.height || 0;
	this.row = opt.row || 1;
	this.col = opt.col || 1;
	this.fps = opt.fps || 1;
	this.url = opt.url || "";
	this.container = opt.container || "";
	this.totalframes = opt.totalframes || 0;
	this.frame = {x: 0, y: 0};
	this.pos = {x: 0,y: 0};
	this.play = qplay;
	this.playreverse = qplayreverse;
	this.pause = qpause;
	this.stop = qstop;
	this.playing = false;
	this.intervalId = null;
	
	var t = document.createElement("div");
	t.setAttribute('id', this.name);
	t.setAttribute('style', `width: ${this.width/this.col}px; height: ${this.height/this.row}px;
background: url('${this.url}') 0px 0px no-repeat;`)
	document.getElementById(this.container).appendChild(t);
}

var circle = document.querySelector('#half-circle');
var c = circle.getTotalLength();

circle.style.strokeDasharray = c + "";
circle.style.strokeDashoffset = c + "";

function setCirclePercentage(num){
    if (num < 0) { num = 0;}
    if (num > 100) { num = 100;}
            
    var pct = ((100-num)/100)*c;
    
    circle.style.strokeDashoffset = pct + "";
}

// baseSprite = new Sprite({
//     container: "webcam",
//     name: "base",
// 	row: 40,
// 	col: 23,
// 	fps: 30,
// 	height: 16000,
// 	width: 16560,
// 	totalframes: 270,
// 	url: "/static/images/base.png"
// });

var messageStack = [];
var queueForIcon = [];
var sprites = {
	donation: {
		sprite: new Sprite({
			container: "webcam",
			name: "coin",
			col: 269,
			row: 1,
			fps: 30,
			height: 60,
			width: 16140,
			totalframes: 269,
			url: "/static/images/coin.png"
		}),
		timeout: 9000,
	},
	subgoal: {
		sprite: new Sprite({
			container: "webcam",
			name: "logosubgoal",
			col: 540,
			row: 1,
			fps: 30,
			height: 60,
			width: 32400,
			totalframes: 540,
			url: "/static/images/logosubgoal.png"
		}),
		timeout: 18000,
	},
	ticket: {
		sprite: new Sprite({
			container: "webcam",
			name: "ticket",
			col: 269,
			row: 1,
			fps: 30,
			height: 60,
			width: 16140,
			totalframes: 269,
			url: "/static/images/ticket.png"
		}),
		timeout: 18000,
	}
}

function spritesIsPlaying(spriteObj){
	for (const key in spriteObj) {
		if (spriteObj.hasOwnProperty(key)) {
			const el = spriteObj[key];
			if(el.sprite.playing)
				return true
		}
	}
	return false
}

function nextAnimation(retry){
	if(queueForIcon.length > 0 && !spritesIsPlaying(sprites)){
		let queItem = queueForIcon.shift();
		for (const s in sprites) {
			if (sprites.hasOwnProperty(s)) {
				const element = sprites[s];
				console.log(element.sprite.name != sprites[queItem].sprite.name);
				if(element.sprite.name != sprites[queItem].sprite.name){
					document.getElementById(element.sprite.name).classList.add("hidden");
				}
			}
		}
		let currentEl = document.getElementById(sprites[queItem].sprite.name);
		if(currentEl.classList.contains("hidden")){
			currentEl.classList.remove("hidden");
		}
		sprites[queItem].sprite.play(1);
		setTimeout(nextAnimation, sprites[queItem].timeout, 2);
	} else if(retry && retry > 0){
		if(queueForIcon.length == 0){
			retry = 0
		} else {
			retry--;
		}
		setTimeout(nextAnimation, 1000, retry);
	}
}

var queue = new QueSprite({
    container: "frame",
    name: "queue",
    col: 90,
    row: 1,
    fps: 30,
    height: 30,
    width: 16200,
    totalframes: 90,
    url: "/static/images/queue.png"
});

queue.play(1);

ws = new ReconnectingWebSocket("ws://localhost:8000/ws");
ws.onmessage = function(evt) { 
    json = JSON.parse(evt.data);
    console.log(json);
    switch (json.type) {
        case "subscriber_count":
			setCirclePercentage(parseInt(json.data.count)/parseInt(json.data.subgoal)*100);
			if(parseInt(json.data.count)/parseInt(json.data.subgoal) >= 1){
				let currentEl = document.getElementById("blur-circle");
				if(currentEl.classList.contains("hidden")){
					currentEl.classList.remove("hidden");
				}
				let lines = document.getElementsByClassName("line");
				for (let i = 0; i < lines.length; i++) {
					const el = lines[i];
					if(!el.classList.contains("show")){
						el.classList.add("show");
					}
				}
				queueForIcon.push("subgoal");
				if(queueForIcon.length <= 1)
					nextAnimation();
			}
			break;
		case "t3emote":
			queueForIcon.push("ticket");
			if(queueForIcon.length <= 1)
				nextAnimation();
			break;
		case "donator":
			queueForIcon.push("donation");
			if(queueForIcon.length <= 1)
				nextAnimation();
			break;
		case "queue":
			if(json.data.show){
				queue.playreverse(1);
			} else {
				queue.play(1);
			}
			break;
    }

    // setTimeout(function(){
    //     console.log(messageStack.shift());
    //     console.log(messageStack);
    // }, 500);
};

