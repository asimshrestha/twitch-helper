function play(repeat = 0){
	if(!this.playing){
		this.playing = true
		this.intervalId = setInterval(() => {
			document.getElementById(this.name).style.backgroundPosition = `-${this.pos.x}px -${this.pos.y}px`;
			
			if(this.totalframes != 0 && (this.frame.x + 1) * (this.frame.y + 1) >= this.totalframes){
				this.pos = {x: 0,y: 0};
				this.frame = {x: 0, y: 0};
				if(repeat > 0){
					repeat--;
					console.log(repeat);
					if(repeat <= 0){
						this.playing = false
                        clearInterval(this.intervalId);
                        this.intervalId = null;
                        this.pos = {
                            x: this.width - (this.width/this.col),
                            y:  this.height - (this.height/this.row)
                        };
                        this.frame = {x: 0, y: 0};
                        document.getElementById(this.name).style.backgroundPosition = `-${this.pos.x}px -${this.pos.y}px`;
					}
				}
			}
			
			if(this.frame.x < this.col - 1){
				this.frame.x++;
				this.pos.x += (this.width/this.col);
			} else {
				this.pos.x = 0;
				
				if(this.frame.y < this.row - 1){
					this.frame.y++;
					this.pos.y += (this.height/this.row);
				} else {
					this.frame.y = 0;
					this.pos.y = 0;
				}
				this.frame.x = 0;
			}
		}, 1000/this.fps);
	}
}

function playreverse(repeat = 0){
	if(!this.playing){
		this.playing = true;
        this.pos.x = this.width - (this.width/this.col);
        this.pos.y = this.height - (this.height/this.row);
		this.intervalId = setInterval(() => {
			document.getElementById(this.name).style.backgroundPosition = `-${this.pos.x}px -${this.pos.y}px`;
			
			if(this.totalframes != 0 && (this.frame.x + 1) * (this.frame.y + 1) >= this.totalframes){
				this.pos = {x: 0,y: 0};
				this.frame = {x: 0, y: 0};
				if(repeat > 0){
					repeat--;
					console.log(repeat);
					if(repeat <= 0){
						this.stop();
					}
				}
			}
			
			if(this.frame.x < this.col - 1){
				this.frame.x++;
				this.pos.x -= (this.width/this.col);
			} else {
				this.pos.x = this.width;
				
				if(this.frame.y < this.row - 1){
					this.frame.y++;
					this.pos.y -= (this.height/this.row);
				} else {
					this.frame.y = 0;
					this.pos.y = this.height;
				}
				this.frame.x = 0;
			}
		}, 1000/this.fps);
	}
}

function pause(){
	if(this.playing){
		this.playing = false
		console.log(this.name + " paused")
		clearInterval(this.intervalId);
		this.intervalId = null;
	}
}

function stop(){
	this.playing = false
	clearInterval(this.intervalId);
	this.intervalId = null;
	this.pos = {x: 0,y: 0};
	this.frame = {x: 0, y: 0};
	document.getElementById(this.name).style.backgroundPosition = "0px 0px";
}

function Sprite(opt = {}){
	this.name = opt.name || opt.container+"img";
	this.width = opt.width || 0;
	this.height = opt.height || 0;
	this.row = opt.row || 1;
	this.col = opt.col || 1;
	this.fps = opt.fps || 1;
	this.url = opt.url || "";
	this.container = opt.container || "";
	this.totalframes = opt.totalframes || 0;
	this.frame = {x: 0, y: 0};
	this.pos = {x: 0,y: 0};
	this.play = play;
	this.playreverse = playreverse;
	this.pause = pause;
	this.stop = stop;
	this.playing = false;
	this.intervalId = null;
	
	var t = document.createElement("div");
	t.setAttribute('id', this.name);
	t.setAttribute('style', `width: ${this.width/this.col}px; height: ${this.height/this.row}px;
background: url('${this.url}') 0px 0px no-repeat;`)
	document.getElementById(this.container).appendChild(t);
}

queue = new Sprite({
    container: "frame",
    name: "queue",
    col: 90,
    row: 1,
    fps: 30,
    height: 30,
    width: 16200,
    totalframes: 90,
    url: "/static/images/queue.png"
});

queue.play(1);
ws = new ReconnectingWebSocket("ws://localhost:8000/ws");
ws.onmessage = function(evt) { 
    json = JSON.parse(evt.data);
    console.log(json);
    if (json.type == "queue"){
        if(json.data.show){
            queue.playreverse(1);
        } else {
            queue.play(1);
        }
    }
};
