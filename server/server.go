package server

import (
	"flag"
	"log"
	"net/http"
	"os"
	"path/filepath"

	"github.com/julienschmidt/httprouter"
	"github.com/urfave/negroni"
)

var hub = newHub()

func StartServer() {
	var dir string
	fN, _ := os.Executable()
	fileDir := filepath.Dir(fN)
	flag.StringVar(&dir, "dir", fileDir+"/server/static/", "the directory to serve files from. Defaults to the static folder")
	flag.Parse()

	go hub.run()

	r := httprouter.New()
	r.GET("/", Index)
	// r.GET("/notification/", Notification)
	r.GET("/layout/", Layout)

	r.GET("/ws", WSHandler)
	// r.GET("/queue/", Queue)
	r.ServeFiles("/static/*filepath", http.Dir(dir))

	n := negroni.New()
	n.Use(negroni.HandlerFunc(SetXPoweredBy))
	n.UseHandler(r)

	srv := &http.Server{
		Handler: r,
		Addr:    ":8000",
	}
	log.Fatal(srv.ListenAndServe())
}

func SetXPoweredBy(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	rw.Header().Set("x-powered-by", "Potato")
	next(rw, r)
}
