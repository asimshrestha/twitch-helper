package server

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/websocket"
	"github.com/julienschmidt/httprouter"
	"gitlab.com/asimshrestha/twitch-helper/helper"
	"gitlab.com/asimshrestha/twitch-helper/save"
)

const (
	// Time allowed to write the file to the client.
	writeWait = 10 * time.Millisecond

	// Time allowed to read the next pong message from the client.
	pongWait = 60 * time.Second

	// Send pings to client with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Poll file for changes with this period.
	filePeriod = 10 * time.Millisecond

	messagePeriod = 10 * time.Millisecond

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var (
	upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}

	newline = []byte{'\n'}
	space   = []byte{' '}

	Msgs []string

	filesLastMod []time.Time
	files        = [][]string{
		{"most_recent_donator.txt", "donator"},
		// {"most_recent_cheerer.txt", "cheerer"},
		// {"most_recent_follower.txt", "follower"},
		// {"most_recent_subscriber.txt", "subscriber"},
		{"session_subscriber_count.txt", "subscriber_count"},
	}
)

func reader(ws *websocket.Conn) {
	defer ws.Close()
	ws.SetReadLimit(512)
	ws.SetReadDeadline(time.Now().Add(pongWait))
	ws.SetPongHandler(func(string) error {
		ws.SetReadDeadline(time.Now().Add(pongWait))
		return nil
	})
	for {
		_, _, err := ws.ReadMessage()
		if err != nil {
			break
		}
	}

}

func writer(ws *websocket.Conn) {
	pingTicker := time.NewTicker(pingPeriod)
	fileTicker := time.NewTicker(filePeriod)
	messageTicker := time.NewTicker(messagePeriod)
	defer func() {
		pingTicker.Stop()
		fileTicker.Stop()
		messageTicker.Stop()
		ws.Close()
	}()

	for {
		select {
		case <-fileTicker.C:
			for i, v := range files {
				if len(filesLastMod) < i+1 {
					filesLastMod = append(filesLastMod, time.Now())
				}
				if save.Setting.StreamLabelsLoc != "" {
					var data []byte
					var fileData string
					var err error
					fileData, filesLastMod[i], err = readFileIfModified(filesLastMod[i], path.Join(save.Setting.StreamLabelsLoc, v[0]))
					if err != nil {
						fmt.Println(err)
						continue
					}
					if fileData != "" {
						var tdata = make(map[string]interface{})
						switch v[1] {
						case "donator", "cheerer":
							splitData := strings.Split(fileData, ":")
							tdata["username"] = strings.TrimSpace(splitData[0])
							tdata["amount"] = strings.TrimSpace(splitData[1])
						case "follower", "subscriber":
							tdata["username"] = strings.TrimSpace(fileData)
						case "subscriber_count":
							var err error
							tdata["count"], err = strconv.Atoi(strings.TrimSpace(fileData))
							if err != nil {
								tdata["count"] = 0
								log.Println(err)
							}
							tdata["subgoal"] = save.Setting.SubGoal
						}
						j, _ := helper.JsonString(v[1], tdata)
						data = []byte(j)
						ws.SetWriteDeadline(time.Now().Add(writeWait))
						if err := ws.WriteMessage(websocket.TextMessage, data); err != nil {
							return
						}
					}
				}
			}
		case <-messageTicker.C:
			var data []byte

			if len(Msgs) != 0 {
				data, Msgs = []byte(Msgs[0]), Msgs[1:]
			} else {
				break
			}

			if data != nil {
				ws.SetWriteDeadline(time.Now().Add(writeWait))
				if err := ws.WriteMessage(websocket.TextMessage, data); err != nil {
					return
				}
			}
		case <-pingTicker.C:
			ws.SetWriteDeadline(time.Now().Add(writeWait))
			if err := ws.WriteMessage(websocket.PingMessage, []byte{}); err != nil {
				return
			}
		}
	}
}

func AddMessage(message string) {
	if message != "" {
		Msgs = append(Msgs, message)
	}
}

func readFileIfModified(lastMod time.Time, filename string) (string, time.Time, error) {
	fi, err := os.Stat(filename)
	if err != nil {
		return "", lastMod, err
	}
	if !fi.ModTime().After(lastMod) {
		return "", lastMod, nil
	}
	p, err := ioutil.ReadFile(filename)
	if err != nil {
		return "", fi.ModTime(), err
	}
	return string(p), fi.ModTime(), nil
}

func WSHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	serveWs(hub, w, r)
}

// 	go writer(ws)
// 	reader(ws)
// }

// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Client is a middleman between the websocket connection and the hub.
type Client struct {
	hub *Hub

	// The websocket connection.
	conn *websocket.Conn

	// Buffered channel of outbound messages.
	send chan []byte
}

// readPump pumps messages from the websocket connection to the hub.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
func (c *Client) readPump() {
	defer func() {
		c.hub.unregister <- c
		c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			break
		}
		message = bytes.TrimSpace(bytes.Replace(message, newline, space, -1))
		c.hub.broadcast <- message
	}
}

// writePump pumps messages from the hub to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *Client) writePump() {
	pingTicker := time.NewTicker(pingPeriod)
	fileTicker := time.NewTicker(filePeriod)
	messageTicker := time.NewTicker(messagePeriod)
	defer func() {
		pingTicker.Stop()
		fileTicker.Stop()
		messageTicker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hub closed the channel.
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			w.Write(message)

			// Add queued chat messages to the current websocket message.
			n := len(c.send)
			for i := 0; i < n; i++ {
				w.Write(newline)
				w.Write(<-c.send)
			}

			if err := w.Close(); err != nil {
				return
			}
		case <-fileTicker.C:
			for i, v := range files {
				if len(filesLastMod) < i+1 {
					filesLastMod = append(filesLastMod, time.Now())
				}
				if save.Setting.StreamLabelsLoc != "" {
					var data []byte
					var fileData string
					var err error
					fileData, filesLastMod[i], err = readFileIfModified(filesLastMod[i], path.Join(save.Setting.StreamLabelsLoc, v[0]))
					if err != nil {
						fmt.Println(err)
						continue
					}
					if fileData != "" {
						var tdata = make(map[string]interface{})
						switch v[1] {
						case "donator", "cheerer":
							splitData := strings.Split(fileData, ":")
							tdata["username"] = strings.TrimSpace(splitData[0])
							tdata["amount"] = strings.TrimSpace(splitData[1])
						case "follower", "subscriber":
							tdata["username"] = strings.TrimSpace(fileData)
						case "subscriber_count":
							var err error
							tdata["count"], err = strconv.Atoi(strings.TrimSpace(fileData))
							if err != nil {
								tdata["count"] = 0
								log.Println(err)
							}
							tdata["subgoal"] = save.Setting.SubGoal
						}
						j, _ := helper.JsonString(v[1], tdata)
						data = []byte(j)
						c.conn.SetWriteDeadline(time.Now().Add(writeWait))
						if err := c.conn.WriteMessage(websocket.TextMessage, data); err != nil {
							return
						}
					}
				}
			}
		case <-messageTicker.C:
			var data []byte

			if len(Msgs) != 0 {
				data, Msgs = []byte(Msgs[0]), Msgs[1:]
			} else {
				break
			}

			if data != nil {
				c.conn.SetWriteDeadline(time.Now().Add(writeWait))
				if err := c.conn.WriteMessage(websocket.TextMessage, data); err != nil {
					return
				}
			}
		case <-pingTicker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

// serveWs handles websocket requests from the peer.
func serveWs(hub *Hub, w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	client := &Client{hub: hub, conn: conn, send: make(chan []byte, 256)}
	client.hub.register <- client

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.writePump()
	go client.readPump()
}
