package helper

func ListContains(arry []string, value string) bool {
	for _, v := range arry {
		if v == value {
			return true
		}
	}
	return false
}
