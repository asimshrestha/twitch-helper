package helper

import (
	"encoding/json"
	"errors"
)

type (
	JSONReturn struct {
		Type string                 `json:"type"`
		Data map[string]interface{} `json:"data"`
	}
)

func JsonString(datatype string, data map[string]interface{}) (string, error) {
	j := JSONReturn{
		Type: datatype,
		Data: data,
	}
	r, err := json.Marshal(j)
	if err != nil {
		return "", errors.New("Error Converting interface")
	}
	return string(r), nil
}
