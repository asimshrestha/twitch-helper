package helper

import (
	"log"
	"os"

	"golang.org/x/sys/windows/registry"
)

func SetReg() {
	filename, _ := os.Executable()
	key, err := registry.OpenKey(registry.CURRENT_USER, `Software\Microsoft\Windows\CurrentVersion\Run`, registry.QUERY_VALUE|registry.SET_VALUE)
	if err != nil {
		log.Println("registry: " + err.Error())
	}
	defer key.Close()
	s, _, err := key.GetStringValue("TwitchHelper")
	if err == registry.ErrNotExist {
		if err := key.SetStringValue("TwitchHelper", filename); err != nil {
			log.Println("registry set: " + err.Error())
		}
		return
	}
	if err != nil {
		log.Println("registry read: " + err.Error())
		return
	}
	if s != filename {
		if err := key.SetStringValue("TwitchHelper", filename); err != nil {
			log.Println("registry set: " + err.Error())
		}
	}
}

func RemoveReg() {
	if IsRegSet() {
		key, err := registry.OpenKey(registry.CURRENT_USER, `Software\Microsoft\Windows\CurrentVersion\Run`, registry.QUERY_VALUE|registry.SET_VALUE)
		if err != nil {
			log.Println("registry: " + err.Error())
		}
		defer key.Close()
		err = key.DeleteValue("TwitchHelper")
		if err != nil {
			log.Println("registry delete value: " + err.Error())
		}
	}
}

func IsRegSet() bool {
	key, err := registry.OpenKey(registry.CURRENT_USER, `Software\Microsoft\Windows\CurrentVersion\Run`, registry.QUERY_VALUE)
	if err != nil {
		log.Println("registry: " + err.Error())
	}
	defer key.Close()
	_, _, err = key.GetStringValue("TwitchHelper")
	if err == registry.ErrNotExist {
		return false
	}
	return true
}
