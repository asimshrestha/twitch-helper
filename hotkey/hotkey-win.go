package hotkey

import (
	"fmt"
	"syscall"
	"time"
	"unsafe"

	"gitlab.com/asimshrestha/twitch-helper/helper"
	"gitlab.com/asimshrestha/twitch-helper/server"
)

type MSG struct {
	HWND   uintptr
	UINT   uintptr
	WPARAM int16
	LPARAM int64
	DWORD  int32
	POINT  struct{ X, Y int64 }
}

var ShowQueue = true

func RegisterHotKey() {
	user32 := syscall.MustLoadDLL("user32")
	defer user32.Release()
	reghotkey := user32.MustFindProc("RegisterHotKey")

	// Hotkeys to listen to:
	keys := map[int16]*Hotkey{
		1: &Hotkey{1, ModAlt + ModCtrl, 'Q'}, // ALT+CTRL+O
	}

	// Register hotkeys:
	for _, v := range keys {
		r1, _, err := reghotkey.Call(
			0, uintptr(v.Id), uintptr(v.Modifiers), uintptr(v.KeyCode))
		if r1 == 1 {
			fmt.Println("Registered", v)
		} else {
			fmt.Println("Failed to register", v, ", error:", err)
		}
	}

	peekmsg := user32.MustFindProc("PeekMessageW")

	for {
		var msg = &MSG{}
		peekmsg.Call(uintptr(unsafe.Pointer(msg)), 0, 0, 0, 1)

		// Registered id is in the WPARAM field:
		if id := msg.WPARAM; id != 0 {
			fmt.Println("Hotkey pressed:", keys[id])
			if id == 1 { // ALT+CTRL+O
				var j = make(map[string]interface{})
				j["show"] = ShowQueue
				ShowQueue = !ShowQueue
				msg, _ := helper.JsonString("queue", j)
				server.AddMessage(msg)
			}
		}

		time.Sleep(time.Millisecond * 50)
	}
}
