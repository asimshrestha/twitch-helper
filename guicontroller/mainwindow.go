package guicontroller

import (
	"github.com/lxn/walk"
	dec "github.com/lxn/walk/declarative"
)

type MyMainWindow struct {
	*walk.MainWindow
	TwitchUsername       *walk.LineEdit
	TwitchEmote          *walk.LineEdit
	OAuth                *walk.LineEdit
	StreamLabelsLocation *walk.LineEdit
	SubGoal              *walk.LineEdit
	SaveBtn              *walk.PushButton

	StartUp *walk.CheckBox
}

var (
	MW = &MyMainWindow{}

	FontTitle = dec.Font{
		Family:    "Arial",
		PointSize: 16,
	}

	FontSubTitle = dec.Font{
		Family:    "Arial",
		PointSize: 14,
	}

	FontSSubTitle = dec.Font{
		Family:    "Arial",
		PointSize: 10,
	}
)
