package guicontroller

import (
	"github.com/lxn/walk"
)

func (mw *MyMainWindow) OpenKeyDialog() {
	dlg, err := walk.NewDialog(mw)
	if err != nil {
		walk.MsgBox(mw, "Error", err.Error(), walk.MsgBoxIconError)
	}

	dlg.SetTitle("Select Hotkey for Queue")
	dlg.SetSize(walk.Size{300, 80})
	go getKey()
	dlg.Run()
}

func getKey() {

}
