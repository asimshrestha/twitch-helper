package twitchchat

import (
	"crypto/tls"
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/asimshrestha/twitch-helper/helper"
	"gitlab.com/asimshrestha/twitch-helper/save"
	"gitlab.com/asimshrestha/twitch-helper/server"

	"github.com/thoj/go-ircevent"
)

const (
	svr  = "irc.chat.twitch.tv"
	port = 6697
)

var (
	ircobj            *irc.Connection
	lastRefreshedTime time.Time
	emoteUsedSubs     []string
)

func IsNotConnected() bool {
	return ircobj == nil || !ircobj.Connected()
}

func StartTracker() {
	ircobj = irc.IRC(save.Setting.Channel, save.Setting.Channel)
	ircobj.Password = save.Setting.OAuth
	// ircobj.Debug = true
	ircobj.UseTLS = true
	ircobj.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	ircobj.Connect(svr + ":" + strconv.Itoa(port))
	ircobj.SendRaw("CAP REQ :twitch.tv/tags twitch.tv/commands twitch.tv/membership")
	ircobj.Join("#" + save.Setting.Channel)
	ircobj.AddCallback("PING", func(e *irc.Event) {
		ircobj.SendRaw("PONG :" + e.Message())
	})
	ircobj.AddCallback("PRIVMSG", func(e *irc.Event) {
		if time.Now().Sub(lastRefreshedTime) >= time.Hour*18 {
			RefreshTimer()
		}
		if e.Tags["subscriber"] == "1" && e.Tags["emotes"] != "" {
			for _, v := range strings.Split(e.Tags["emotes"], "/") {
				if v != "" {
					var erange []int
					emote := strings.Split(v, ":")
					if strings.Contains(emote[1], ",") {
						var t int
						ers := strings.Split(strings.Split(emote[1], ",")[0], "-")
						t, _ = strconv.Atoi(ers[0])
						erange = append(erange, t)
						t, _ = strconv.Atoi(ers[1])
						erange = append(erange, t)
					} else {
						var t int
						ers := strings.Split(emote[1], "-")
						t, _ = strconv.Atoi(ers[0])
						erange = append(erange, t)
						t, _ = strconv.Atoi(ers[1])
						erange = append(erange, t)
					}
					if e.Message()[erange[0]:erange[1]+1] == save.Setting.Emote &&
						!helper.ListContains(emoteUsedSubs, e.Nick) {
						emoteUsedSubs = append(emoteUsedSubs, e.Nick)

						var tm = make(map[string]interface{})
						tm["username"] = e.Nick

						msg, _ := helper.JsonString("t3emote", tm)
						fmt.Println(emoteUsedSubs, msg)
						server.AddMessage(msg)
						return
					}
				}
			}
		}
	})
	ircobj.Loop()
}

func RestartTracker() {
	ircobj.Quit()
	StartTracker()
}

func RefreshTimer() {
	lastRefreshedTime = time.Now()
	emoteUsedSubs = []string{}
	fmt.Println("Timer Reset")
}
