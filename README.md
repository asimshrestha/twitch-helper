# Twitch Helper

Allows you to track stats from streamlabels and track specified emotes from your chat.

## Setting

![Settings](https://gitlab.com/asimshrestha/twitch-helper/raw/master/screenshot/settingexample.png)

1. Twitch Channel
    * This is the Twitch Channel name that will be tracked for.
2. Emote Name
    * The Emote name that would be tracked in chat
3. Oauth Code
    * Code for login in to chat
    * [Link for Oauth Code](https://twitchapps.com/tmi/)
4. Stream Labels Path/Location
    * Folder location for Streamlabels
    * Ex: 'C:\Users\\\<username\>\\Documents\streamlabs'
5. Sub Goal
    * Your Sub Goal for your stream

## Tray Options

![Tray Settings](https://gitlab.com/asimshrestha/twitch-helper/raw/master/screenshot/trayicon.png)

1. Hide Settings
    * Hides the Setting Option
2. Exit
    * Exits the program