package save

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"

	"gopkg.in/ini.v1"
)

const (
	configFile = "settings.ini"
)

type (
	Settings struct {
		Channel         string
		Emote           string
		OAuth           string
		StreamLabelsLoc string
		SubGoal         int
	}
)

var (
	CFG     *ini.File
	Setting Settings
)

func (s *Settings) IsMissingValues() bool {
	return s.Channel == "" && s.Emote == "" && s.OAuth == "" && s.StreamLabelsLoc == "" && s.SubGoal == 0
}

func (s *Settings) IsEqual(s2 Settings) bool {
	return s.Channel == s2.Channel && s.Emote == s2.Emote && s.OAuth == s2.OAuth && s.StreamLabelsLoc == s.StreamLabelsLoc && s.SubGoal == s2.SubGoal
}

func SaveConfig() {
	fN, _ := os.Executable()
	fileDir := filepath.Dir(fN)
	CFG.Section("twitch").Key("channel").SetValue(Setting.Channel)
	CFG.Section("twitch").Key("emote").SetValue(Setting.Emote)
	CFG.Section("twitch").Key("oauth").SetValue(Setting.OAuth)
	CFG.Section("config").Key("streamlabelsloc").SetValue(Setting.StreamLabelsLoc)
	CFG.Section("config").Key("subgoal").SetValue(strconv.Itoa(Setting.SubGoal))
	CFG.SaveTo(fileDir + `\` + configFile)
}

func LoadSettings() {
	fN, _ := os.Executable()
	fileDir := filepath.Dir(fN)
	var err error
	CFG, err = ini.Load(fileDir + `\` + configFile)
	if err != nil {
		fmt.Println("Error: ", err)
		SetUpSettings()
	} else {
		var err error
		Setting.Channel = CFG.Section("twitch").Key("channel").String()
		Setting.Emote = CFG.Section("twitch").Key("emote").String()
		Setting.OAuth = CFG.Section("twitch").Key("oauth").String()
		Setting.StreamLabelsLoc = CFG.Section("config").Key("streamlabelsloc").String()
		Setting.SubGoal, err = CFG.Section("config").Key("subgoal").Int()

		if err != nil {
			log.Fatal(err)
		}

		fmt.Println(Setting)

		// guicontroller.MW.TwitchUsername.SetText("ASD")
		// guicontroller.MW.TwitchEmote.SetText(Setting.Emote)
		// guicontroller.MW.OAuth.SetText(Setting.OAuth)
	}
}

func SetUpSettings() {
	fN, _ := os.Executable()
	fileDir := filepath.Dir(fN)

	cfg := ini.Empty()
	cfg.Section("twitch").Key("channel").SetValue("")
	cfg.Section("twitch").Key("emote").SetValue("")
	cfg.Section("twitch").Key("oauth").SetValue("")
	cfg.Section("config").Key("streamlabelsloc").SetValue("")
	cfg.Section("config").Key("subgoal").SetValue("")
	cfg.SaveTo(fileDir + `\` + configFile)
	CFG = cfg
}
